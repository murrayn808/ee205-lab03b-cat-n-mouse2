#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

GUESS=-1

while [[ $GUESS -ne $THE_NUMBER_IM_THINKING_OF ]]
do

   printf "OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:\n"

   read GUESS

   if [[ $GUESS -lt 1 ]]
   then

      printf "You must enter a number that's >= 1\n"


   elif [[ $GUESS -gt $THE_MAX_VALUE ]] 
   then

      printf "You must enter a number that's <= $THE_MAX_VALUE\n"

   elif [[ $GUESS -gt $THE_NUMBER_IM_THINKING_OF ]]
   then

      printf "No cat... the number I’m thinking of is smaller than $GUESS\n"

   elif [[ $GUESS -lt $THE_NUMBER_IM_THINKING_OF ]]
   then

      printf "No cat... the number I’m thinking of is larger than $GUESS\n"

   else 
      echo "You got me."
      printf "          |\\___/| \n"
      printf "         =) ^ ^ (= \n"
      printf "          \\  ^  / \n"
      printf "           )=*=( \n"
      printf "          /     \\ \n"
      printf "          |     |\\ \n"
      printf "         /| | | |\\ \n"
      printf "         \\| | |_|/\\ \n"
      printf "          |_|_|_|_/ \n"

   fi

done
